//
//  AppDelegate.h
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 21/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

