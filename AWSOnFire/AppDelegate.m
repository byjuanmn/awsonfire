//
//  AppDelegate.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 21/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>
#import <AWSMobileAnalytics/AWSMobileAnalytics.h>

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    [AWSLogger defaultLogger].logLevel = AWSLogLevelDebug;
    
    AWSMobileAnalytics *analytics = [AWSMobileAnalytics
                                     mobileAnalyticsForAppId: @"be75bea9802240c6a0e8914a04a3f63c" //Amazon Mobile Analytics App ID
                                     identityPoolId: @"us-east-1:7854985e-3b8e-44c9-b4e0-3629590594de"]; //Amazon Cognito Identity Pool ID
    
    
    
    id<AWSMobileAnalyticsEventClient> eventClient = analytics.eventClient;
    
    id<AWSMobileAnalyticsEvent> levelEvent = [eventClient createEventWithEventType:@"EjemploComplete"];
    
    [levelEvent addAttribute:@"ClaveEventoEjemplo" forKey:@"EventoPrimario"];
    [levelEvent addMetric:@123 forKey:@"Medida"];
    
    [eventClient recordEvent:levelEvent];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    
    return YES;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}





- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
