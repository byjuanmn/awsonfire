//
//  DynamoDBViewController.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 23/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//


#import <AWSCore.h>
#import <AWSDynamoDB/AWSDynamoDB.h>


#import "OnFirePersonas.h"

#import "DynamoDBViewController.h"

@interface DynamoDBViewController () {
    
    AWSDynamoDBObjectMapper * mapper;
    AWSDynamoDBObjectMapperConfiguration *configuration;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DynamoDBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupDynamoDB];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)addItem:(id)sender {
    
    [self addNewDynamoDBObject];
}

- (IBAction)queryItems:(id)sender {
    [self selectAllRecordsFromDynomoDB];
}

#pragma mark - DynamoDB 


- (void) setupDynamoDB{
    
    mapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    // configuracion
    
}


- (void) addNewDynamoDBObject{
    
    OnFirePersonas * persona = [OnFirePersonas new];
    
    // rellenar sus atributos
    
    persona.idPersona =[[NSUUID UUID]UUIDString];
    persona.age = @23;
    persona.namePerson = @"Juan";
    
   
    [[mapper save:persona]continueWithBlock:^id(AWSTask *task) {
       
        if (task.error) {
            
            NSLog(@"Tenemos un error ---> %@", task.error);
            
        }
        
        if (task.exception) {
            NSLog(@"Tenemos una excepcion ---> %@", task.exception);
        }
        
        if (task.result) {
            NSLog(@"Y el resultado es -> %@", task.result);
        }
        
        return nil;
    }];
    
}


- (void) selectAllRecordsFromDynomoDB{
    
    AWSDynamoDBScanExpression * expression = [AWSDynamoDBScanExpression new];
    
    expression.limit = @5;
    expression.filterExpression = @"age < :val";
    expression.expressionAttributeValues = @{@":val":@23};
    
    [[mapper scan:[OnFirePersonas class] expression:expression] continueWithBlock:^id(AWSTask *task) {
       
        if (task.error) {
            
            NSLog(@"Tenemos un error ---> %@", task.error);
            
        }
        
        if (task.exception) {
            NSLog(@"Tenemos una excepcion ---> %@", task.exception);
        }
        
        if (task.result) {

            NSLog(@"%@", task.result);
            AWSDynamoDBPaginatedOutput *pagingOutput = task.result;
            
            for (OnFirePersonas *item in pagingOutput.items) {
                
                NSLog(@"El nombre -> %@ \t %@ \t %@", item.namePerson, item.idPersona, item.age);
            }
            
            
        }

        return nil;
        
    }];
    
}
































/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
