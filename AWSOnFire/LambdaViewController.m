//
//  LambdaViewController.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 23/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//


#import <AWSCore/AWSCore.h>
#import <AWSLambda/AWSLambda.h>

#import "LambdaViewController.h"

@interface LambdaViewController ()

@end

@implementation LambdaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)execLambdaAction:(id)sender {
    
    [self triggerMyLambda];
}


#pragma mark - Lambda Section


- (void) triggerMyLambda{
    
    AWSLambdaInvoker * lambdaInvoker = [AWSLambdaInvoker defaultLambdaInvoker];
    
    
    
    [[lambdaInvoker invokeFunction:@"HelloWorld1" JSONObject:@{@"clave1":@"valor de la clave1"}] continueWithBlock:^id(AWSTask *task) {
       
        if (task.error) {
            NSLog(@"Error -> %@", task.error);
            
        } else {
            
            NSLog(@"%@", task.result);
        }
        
        
        return nil;
    }];
    
    
    
    
    
}




























@end
