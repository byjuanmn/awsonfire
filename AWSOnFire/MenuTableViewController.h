//
//  MenuTableViewController.h
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 22/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewController : UITableViewController

@end
