//
//  MenuTableViewController.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 22/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import "MenuTableViewController.h"

@interface MenuTableViewController () {
    NSArray *modelMenu;
}



@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self populateMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - menu

- ( void)populateMenu{
    
    modelMenu = @[@"Demo Cognito", @"Demo S3", @"Demo Lambda", @"Demo DynamoDB"];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return modelMenu.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELDA" forIndexPath:indexPath];
    
    
    cell.textLabel.text = modelMenu[indexPath.row];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"DemoCognito" sender:nil];
            break;
        case 1:
            [self performSegueWithIdentifier:@"DemoS3" sender:nil];
            break;
        case 2:
            [self performSegueWithIdentifier:@"DemoLambda" sender:nil];
            break;
            
        case 3:
            [self performSegueWithIdentifier:@"DemoDynamoDB" sender:nil];
            break;
            
        default:
            break;
    }
}


@end
