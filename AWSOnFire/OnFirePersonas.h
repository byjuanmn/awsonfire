//
//  OnFirePersonas.h
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 23/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSDynamoDB/AWSDynamoDB.h>


@interface OnFirePersonas : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (strong, nonatomic) NSString * idPersona;

@property (strong, nonatomic) NSString * namePerson;
@property (strong, nonatomic) NSNumber * age;



@end
