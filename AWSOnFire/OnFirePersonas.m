//
//  OnFirePersonas.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 23/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import "OnFirePersonas.h"

@implementation OnFirePersonas

+(NSString *) dynamoDBTableName{
    return @"OnFirePersonas";
}

+(NSString *)hashKeyAttribute{
    return @"idPersona";
}

@end
