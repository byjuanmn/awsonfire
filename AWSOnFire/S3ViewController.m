//
//  S3ViewController.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 22/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import "S3ViewController.h"

#import <AWSS3/AWSS3.h>
#import <AWSCognito/AWSCognito.h>
#import <AWsCore/AWSCore.h>



@interface S3ViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    
    AWSCognitoCredentialsProvider *credentialsProvider;
    AWSCognito *clientSync;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;


@end

@implementation S3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupAWS];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)downloadAction:(id)sender {
    
    [self downloadBlobFromBucket];
}

- (IBAction)upLoadAction:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
    
   
}


#pragma mark - Picker

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage * image = info[UIImagePickerControllerOriginalImage];
    self.imageView.image = image;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    

    [self uploadBlobInBucket:image];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    NSLog(@"Error en la seleccion de la imagen");
}



#pragma mark - AWS Cognito

- (void)setupAWS{
    
    credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                           initWithRegionType:AWSRegionEUWest1
                           identityPoolId:@"eu-west-1:f14bbc03-a1b9-46b8-be3f-5668c110d046"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionEUWest1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;

}

#pragma mark - AWS S3 upload

- (void)uploadBlobInBucket:(UIImage *)image{
    
    NSString * pathFileLocal = [self generateUrlToLocalFile:image];
    
    
    AWSS3TransferManager * uploadManager = [AWSS3TransferManager defaultS3TransferManager];
    
    AWSS3TransferManagerUploadRequest *upRequest = [AWSS3TransferManagerUploadRequest new];
    
    
    upRequest.bucket = @"awsonfire";
    upRequest.key = [pathFileLocal lastPathComponent]; //nombre del blob
    upRequest.body = [[NSURL alloc] initFileURLWithPath:pathFileLocal];
    upRequest.contentType = @"image/jpeg";
    
    upRequest.uploadProgress = ^(int64_t bytesRead, int64_t totalBytesRead, int64_t totalBytesExpectedToRead){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            double progress = (double)(totalBytesRead / 1024) / (double)(totalBytesExpectedToRead /1024);
            NSLog(@"Subiendo -----> %.f", progress);
            
            [self.progressView setProgress:progress animated:YES];
            if (self.progressView.progress >=1 ) {
                [self.progressView setHidden:YES];
            }
            
        });
    };
    
    
    [[uploadManager upload:upRequest]continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        
        if (task.error) {
            // error
            NSLog(@"error --> %@", task.error);
            
        } else {
            
            // exito
        }
     
        return nil;
    }];
    
    
    
    
    
}

// metodo de ayuda para generar y almacenar la imagen y subirla a S3
- (NSString *) generateUrlToLocalFile:(UIImage *) image {
    
    // nombre del fichero
    NSString * imageName = [NSString stringWithFormat:@"%@.jpeg", [[NSUUID UUID]UUIDString]];
    
    //ruta
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString * documentDir = path[0];
    
    NSString *fullPath = [[NSString alloc]initWithString:[documentDir stringByAppendingPathComponent:imageName]];
    
    NSData * buffer = UIImageJPEGRepresentation(image, 1);
    
    NSError *error;
    [buffer writeToFile:fullPath options:NSDataWritingAtomic error:&error];
    
    
    return fullPath;
}

#pragma mark - AWS S3 Download


- (void)downloadBlobFromBucket {
    
    // path del fichero
    NSString * downloadPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"foto.jpeg"];
    
    NSURL * urlLocal = [NSURL fileURLWithPath:downloadPath];
    
    
    AWSS3TransferManager * manager = [AWSS3TransferManager defaultS3TransferManager];
    
    AWSS3TransferManagerDownloadRequest * request = [AWSS3TransferManagerDownloadRequest new];
    
    request.bucket = @"awsonfire";
    request.key = @"winter-is-coming.jpg";
    request.downloadingFileURL = urlLocal;
    
    request.downloadProgress = ^(int64_t bytesRead, int64_t totalBytesRead, int64_t totalBytesExpectedToRead){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            double progress = (double)(totalBytesRead / 1024) / (double)(totalBytesExpectedToRead /1024);
            NSLog(@"Subiendo -----> %.f", progress);
            
            [self.progressView setProgress:progress animated:YES];
            if (self.progressView.progress >=1 ) {
                [self.progressView setHidden:YES];
            }
            
        });
    };

    
    [[manager download:request]continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
     
        
        if (task.error) {   
            if([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]){
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorPaused:
                    case AWSS3TransferManagerErrorCancelled:
                        break;
                        
                    default:
                        NSLog(@"Error ----> %@", task.error);
                        break;
                }
            } else {
                NSLog(@"Error ----> %@", task.error);
            }
        }
        
        if (task.result) {
            
            AWSS3TransferManagerDownloadOutput * salida = task.result;
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:salida.body]];
            
            self.imageView.image = image;
            
        }
        
        
        
        return nil;
    }];
    
    
    
}






















/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
