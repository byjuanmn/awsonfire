//
//  ViewController.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 21/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <AWSMobileAnalytics/AWSMobileAnalytics.h>


#import "ViewController.h"

@interface ViewController () {
    AWSCognitoCredentialsProvider *credentialsProvider;
    
    AWSMobileAnalytics *analytics;
    
    id<AWSMobileAnalyticsEventClient> eventClient;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [self setupMB];
    
    
    credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                          initWithRegionType:AWSRegionEUWest1
                                                          identityPoolId:@"eu-west-1:f14bbc03-a1b9-46b8-be3f-5668c110d046"];

    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionEUWest1
                                                                         credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileUserData:)
                                                 name:FBSDKProfileDidChangeNotification
                                               object:nil];
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        // llamar a una funcion propia para logarnos en AWS con el token actual
        NSLog(@"Usuario Logado : %@", [FBSDKAccessToken currentAccessToken].tokenString);
        
        [self loginFacebookUserInCognito];
    }
    
    
}


- (void)profileUserData:(id) sender{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MB Events

- (void)setupMB{
    analytics = [AWSMobileAnalytics mobileAnalyticsForAppId: @"be75bea9802240c6a0e8914a04a3f63c" //Amazon Mobile Analytics App ID
                                             identityPoolId: @"us-east-1:7854985e-3b8e-44c9-b4e0-3629590594de"]; //Amazon Cognito Identity Pool ID
    eventClient = analytics.eventClient;

}


- (void)analyticsEventForCognitoInfoSync{
    
    id<AWSMobileAnalyticsEvent> cognitoEvent = [eventClient createEventWithEventType:@"CognintoSyc"];
    
    [cognitoEvent addAttribute:@"Sincronizado" forKey:@"Sync"];
    
    [eventClient submitEvents];
}



- (void)analyticsEventForUserLoginInFB{
    
    
    id<AWSMobileAnalyticsEvent> levelEvent = [eventClient createEventWithEventType:@"LoingInFB"];
    
    [levelEvent addAttribute:[FBSDKProfile currentProfile].name
                      forKey:@"UserNameFB"];

    [levelEvent addAttribute:[FBSDKAccessToken currentAccessToken].userID forKey:@"userIdFB"];
    
    
    
    
    [eventClient recordEvent:levelEvent];

    
    
    
    
}


#pragma mark - IBAction FB

- (IBAction)loginFacebookButton:(id)sender {
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc]init];
    
    [loginManager logInWithReadPermissions:@[@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            NSLog(@"Error -> %@", error);
        } else if (result.isCancelled){
            NSLog(@"Login cancelado");
        } else {
            [self loginFacebookUserInCognito];
        }
        
    }];
    
    
}



- (void)loginFacebookUserInCognito{
    
    NSString *token = [FBSDKAccessToken currentAccessToken].tokenString;
    credentialsProvider.logins = @{ @(AWSCognitoLoginProviderKeyFacebook) : token };
    
    NSLog(@"Creditials ---> %@ \t %@ \t %@", credentialsProvider.identityId, credentialsProvider.identityPoolId, credentialsProvider);
    
    [[credentialsProvider getIdentityId] continueWithSuccessBlock:^id(AWSTask *task) {

        
        if (task.error) {
            NSLog(@"%@", task.error.domain);
        } else {
            NSLog(@"Creditials OK ---> %@ \t %@ \t %@", credentialsProvider.identityId, credentialsProvider.identityPoolId, credentialsProvider);
            
            [self analyticsEventForUserLoginInFB];
            
            [self syncUserDataInCognitoProfile];
            
        }
        
        
        return nil;
        
    }];
     
    
}


- (void)syncUserDataInCognitoProfile{
    
    AWSCognito *clientCog = [AWSCognito defaultCognito];
    
    
    AWSCognitoDataset * dataSet = [clientCog openOrCreateDataset:@"UserInfoFromAWSOnFire"];
    
    [dataSet setString:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"token_user"];
    
    [dataSet setString:[FBSDKProfile currentProfile].name forKey:@"userName"];
    
    [dataSet setString:[FBSDKAccessToken currentAccessToken].userID forKey:@"userId"];
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
    [dateFormater setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
    
    [dataSet setString:[dateFormater stringFromDate:[NSDate date]] forKey:@"lastLogin"];
    

    [[dataSet synchronize] continueWithBlock:^id(AWSTask *task) {
        
        if (!task.error) {
            NSLog(@"dataSet sincronizado");
            
            [self analyticsEventForCognitoInfoSync];
        } else {
            NSLog(@"El error es %ld \t %@", (long)task.error.code, task.error.domain);
        }
        
        return nil;
        
    }];
    
    
}









































@end
