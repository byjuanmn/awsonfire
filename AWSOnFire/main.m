//
//  main.m
//  AWSOnFire
//
//  Created by Juan Antonio Martin Noguera on 21/10/15.
//  Copyright © 2015 Cloud On Mobile S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
